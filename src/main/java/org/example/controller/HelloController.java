package org.example.controller;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/hello")
public class HelloController {

    @GetMapping("/getHello")
    public String getHello(){
        return "hello";
    }

    @GetMapping("/admin/getAdminHello")
    public String getHelloAdmin(){
        return "hello admin !!";
    }

    @GetMapping("/admin/getEncryptedPassword/{password}")
    public String getpassword(@PathVariable("password")String password){
        return new BCryptPasswordEncoder().encode(password);
    }
}

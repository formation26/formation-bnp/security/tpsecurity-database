package org.example.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import javax.sql.DataSource;

@Configuration
public class SecurityConfiguration  {

    @Autowired
    private DataSource datasource;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,"/api/v1/hello/getHello").hasRole("USER")
                .antMatchers(HttpMethod.GET,"/api/v1/hello/admin/*").hasRole("ADMIN")
                .and()
                .csrf()
                .disable()
                .formLogin().disable();
        return http.build();
    }
   @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

       auth.jdbcAuthentication()
               .dataSource(datasource)
               .passwordEncoder(new BCryptPasswordEncoder())
               .usersByUsernameQuery("select username,password, enabled from users where username=? ")
               .authoritiesByUsernameQuery("select username, authority from authorities where username=? ");

   }
}
